
#include "cuda_runtime.h"
#include "device_launch_parameters.h"
#include <cuda.h>
#include <stdio.h>

void doRGBToGrey(unsigned char* h_input, unsigned char* h_output, int w, int h);

__global__ void rgbToGrey(unsigned char* d_input, unsigned char* d_output, int w, int h){
	int i = blockIdx.x;
	int j = blockIdx.y;
	int index = w*j + i;
	d_output[index] = 0.299f * d_input[4*index] + 0.587f * d_input[4*index + 1] + 0.114f * d_input[4*index + 2];
}

void doRGBToGrey(unsigned char* h_input, unsigned char* h_output, int w, int h){

	int BYTES_IN = 4 * w*h*sizeof(unsigned char);
	int BYTES_OUT = BYTES_IN / 4;
	dim3 blocks = dim3(w, h, 1);
	unsigned char* d_input;
	unsigned char* d_output;
	cudaMalloc((void**)&d_input, BYTES_IN);
	cudaMalloc((void**)&d_output, BYTES_OUT);

	cudaMemcpy((void**)d_input, (void**)h_input, BYTES_IN, cudaMemcpyHostToDevice);
	rgbToGrey<<<blocks, 1>>>(d_input, d_output, w, h);
	cudaMemcpy((void**)h_output, (void**)d_output, BYTES_OUT, cudaMemcpyDeviceToHost);

}
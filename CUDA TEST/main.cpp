#include "lodepng.h"

#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include </Users/Alejandro Penagos/Documents/Visual Studio 2013/Projects/CUDA TEST/CUDA TEST/kernel.h>
#include <chrono>


void loadImg(const char* name, std::vector<unsigned char> &img, std::vector<unsigned char> &img_raw, unsigned int &w, unsigned int &h, bool debug);
void rgb_to_gray(std::vector<unsigned char> in_img, std::vector<unsigned char> &out_img, unsigned int w, unsigned int h, bool debug);
void rgb_to_gray_CUDA(std::vector<unsigned char> img_in, std::vector<unsigned char> &out_img, unsigned int w, unsigned int h, bool debug);
void saveImg(const char* name, std::vector<unsigned char> img, lodepng::State state, unsigned int w, unsigned int h, bool debug);
void testCUDA(const char* inName, const char* outName, lodepng::State state);
void testCPU(const char* inName, const char* outName, lodepng::State state);

bool debug = false;

std::vector<unsigned char> img;
std::vector<unsigned char> img_raw;
std::vector<unsigned char> gray;
unsigned int w, h;

int main(){
	lodepng::State state;

	state.info_png.color.colortype = LCT_GREY;
	state.info_raw.colortype = LCT_GREY;
	state.encoder.auto_convert = 0;

	const char* inName = "C:/Users/Alejandro Penagos/Desktop/p4.png";
	const char* outCUDA = "C:/Users/Alejandro Penagos/Desktop/test_CUDA.png";
	const char* outCPU = "C:/Users/Alejandro Penagos/Desktop/test_CPU.png";

	testCUDA(inName, outCUDA, state);
	testCPU(inName, outCPU, state);

	std::cin.get();

	return 0;
}

void loadImg(const char* name, std::vector<unsigned char> &img, std::vector<unsigned char> &img_raw, unsigned int &w, unsigned int &h, bool debug){
	lodepng::load_file(img_raw, name);
	unsigned error = lodepng::decode(img, w, h, img_raw);
	if (error) std::cout << "Decoder error (" << error << "): " << lodepng_error_text(error) << std::endl;
	if (debug) printf("W: %d\nH: %d\n", w, h);
}

void rgb_to_gray(std::vector<unsigned char> in_img, std::vector<unsigned char> &out_img, unsigned int w, unsigned int h, bool debug){
	
	if(debug) printf("Vector:\n");
	out_img.resize((int)round((in_img.size()) / 4));
	// Prepare the data	
	int where = 0;
	float sum = 0;
	for (int i = 0; i < in_img.size(); i++) {
		int mod = i % 4;
		if (mod != 3) {
			int value = in_img.at(i);
			
			switch (mod){
			case 0: //R
				sum += 0.299f * in_img.at(i);
				break;
			case 1: //G
				sum += 0.587f * in_img.at(i);
				break;
			case 2: //B
				sum += 0.114f * in_img.at(i);
				break;
			default:
				break;
			}

			if (debug) std::cout << "[" << (int)in_img.at(i) << ", " << mod << "] -- ";

		}
		else {
			if(debug) std::cout << "[[" << sum << "]]\n";
			out_img[(int)round(i / 4)] = sum;
			sum = 0;
		}
		//std::cin.get();
	}
}

void saveImg(const char* name, std::vector<unsigned char> img, lodepng::State state, unsigned int w, unsigned int h, bool debug){
	std::vector<unsigned char> aux;
	unsigned error = lodepng::encode(aux, img, w, h, state);
	lodepng::save_file(aux, name);
	if (error) std::cout << "encoder error " << error << ": " << lodepng_error_text(error) << std::endl;
	if (debug) printf("Saved %S", name);
}


void rgb_to_gray_CUDA(std::vector<unsigned char> img_in, std::vector<unsigned char> &out_img, unsigned int w, unsigned int h, bool debug){
	unsigned char* in = &img_in[0];
	out_img.resize((int)round(img_in.size() / 4));
	unsigned char* out = &out_img[0];
	doRGBToGrey(in, out, w, h);
}

void testCUDA(const char* inName, const char* outName, lodepng::State state){
	loadImg(inName, img, img_raw, w, h, debug);
	auto t1 = std::chrono::high_resolution_clock::now();

	rgb_to_gray_CUDA(img, gray, w, h, debug);

	auto t2 = std::chrono::high_resolution_clock::now();
	saveImg(outName, gray, state, w, h, debug);

	std::cout << "CUDA: " << std::chrono::duration_cast<std::chrono::milliseconds>(t2 - t1).count() << "ms" << std::endl;

}

void testCPU(const char* inName, const char* outName, lodepng::State state){
	loadImg(inName, img, img_raw, w, h, debug);
	auto t1 = std::chrono::high_resolution_clock::now();

	rgb_to_gray(img, gray, w, h, debug);

	auto t2 = std::chrono::high_resolution_clock::now();
	saveImg(outName, gray, state, w, h, debug);

	std::cout << "CPU: " << std::chrono::duration_cast<std::chrono::milliseconds>(t2 - t1).count() << "ms" << std::endl;

}